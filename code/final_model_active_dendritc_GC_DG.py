#!/usr/bin/env python3

###############################################################################
#                           AdaptiveIF model BRIAN2                           #
#                               active dendrite                               #
#                                    via                                      #
#                                custom events                                #
#                                with SYNAPSES                                #
#                            FINAL MODEL  (Poisson)                           #
###############################################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

# factor function used for normalization and min(=0), max(=1) during s_{AMPA,NMDA} graphs.
def factor(tau_rise,tau_decay):
    # Calculate the time @ peak
    tpeak = ((tau_decay*tau_rise) / (tau_decay-tau_rise)) * np.log(tau_decay/tau_rise)
    # Calculate the peak value - maximum value
    smax = ((tau_decay*tau_rise) / (tau_decay-tau_rise))*( np.exp(-(tpeak)/tau_decay) - np.exp(-(tpeak)/tau_rise) ) / ms
    
    return (1/smax)


###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

start_scope()

## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -80 * mV
Csoma 	= 70.* pfarad
glsoma 	= (Csoma / (26.9 * ms)) / 2.3
Vreset  = -74. * mV
Vthres  = -56. * mV 

## dendrite
Eldend  = (Elsoma + 5 * mV)
gldend  = 2. * glsoma
Cdend   = 2. * Csoma

## Global
gc      = 6.8 * nS

## AdIF
alpha   = 1.35 * nS
tauw    = 45. * ms
beta    = .045 * nA

## Na & K channels
tauNa   = 3. * ms
tauK = 6. * ms

### AMPA (excitatory)
E_AMPA      = 0. * mV
g_AMPA_ext  = 2.1066 * nS
tau_AMPA    = 2. * ms
tau_AMPA_rise     = .073 * ms
tau_AMPA_decay    = 2. * ms
alpha_AMPA        = .68 / ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = .99 * g_AMPA_ext
tau_NMDA_rise     = .33 * ms
tau_NMDA_decay    = 50. * ms
alpha_NMDA        = .22 / ms
Mg2               = 2.
heta              = .2
gamma             = .04

## Model irrelevant
tstart = 600 * ms
tend   = 800 * ms
fampa  = factor(tau_AMPA_rise,tau_AMPA_decay)
fnmda  = factor(tau_NMDA_rise,tau_NMDA_decay)

###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma ) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + IK + INa + Iextdend - Isyn) / Cdend :volt
    dINa/dt = -INa / tauNa : amp
    dIK/dt = -IK / tauNa : amp
    
    Isyn = I_AMPA_ext + I_NMDA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext_tot : amp
    s_AMPA_ext_tot : 1
    
    I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + Mg2 / heta * exp(gamma * Vdend / mV)) * s_NMDA_ext_tot : amp
    s_NMDA_ext_tot : 1 
    
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''

granule = NeuronGroup(1, model = eqs_gc, 
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory= 2. * ms,
                     events = {
                     'dspike' : 'Vdend > -55 * mV and allow_dspike',
                     'drop_dspike' : 'Vdend > -37 * mV and not allow_dspike',
                     'force_true_dspike' : 'IK > -.009 * pA and not allow_dspike',
                     },
                     method = 'rk4'
                     )

# custom events conditions
granule.run_on_event('dspike', 'INa = 1000 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'IK = - 1600 * pA')
granule.run_on_event('force_true_dspike', 'allow_dspike = True')

###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################

custom_rates = 10 * Hz
spike_inp = PoissonGroup(42, rates = 0 * Hz)

synGlut = Synapses(spike_inp, granule,
                   model = """
                   s_AMPA_ext_tot_post = s_AMPA_ext : 1 (summed)
                   ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + alpha_AMPA * x_AMPA * (1 - s_AMPA_ext) : 1 (clock-driven)
                   dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1 (clock-driven)
                   
                   s_NMDA_ext_tot_post = s_NMDA_ext : 1 (summed)
                   ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + alpha_NMDA * x_NMDA * (1 - s_NMDA_ext) : 1 (clock-driven)
                   dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 (clock-driven)
                   """,
                   on_pre = 'x_AMPA += 1; x_NMDA += 1',
                   method = 'rk4'
                   )

synGlut.connect()

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

spk = SpikeMonitor(granule)

# recording/monitoring variables, spikes and whatever needed.
mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'I_AMPA_ext', 'I_NMDA_ext', 's_AMPA_ext_tot', 's_NMDA_ext_tot', 'allow_dspike', "INa", "IK"), record=True)

# We'll divide our experiment in 3 runs to be able to start and drop Poisson rates to 0 Hz.
spike_inp.rates = 0 * Hz
run(300 *ms, report = 'text')

spike_inp.rates = custom_rates
granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(700 * ms, report = 'text')

spike_inp.rates = 0 * Hz
granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(500 * ms, report = 'text')

nstart      = int(tstart/defaultclock.dt)
nend        = int(tend/defaultclock.dt)

###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################

EPSP_amp = max(mon[0].Vsoma[nstart:nend]) - min(mon[0].Vsoma[nstart:nend])

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

### Nicer somatic spikes
Vsoma_hack = mon[0].Vsoma[:]
for t in spk.t : 
      some_value = int(t / defaultclock.dt)
      Vsoma_hack[some_value] = 20 * mV

# Specific part of dendritic EPSP plot
plt.subplot(423)
plt.plot(mon[0].t[nstart:nend] / ms, Vsoma_hack[nstart:nend] / mV, label='soma', c='black')
plt.ylabel('EPSP (mV)', fontsize = 12)
plt.legend()

# Specific part of dendritic EPSP plot
plt.subplot(424)
plt.plot(mon[0].t[nstart:nend] / ms, mon[0].Vdend[nstart:nend] / mV, label='dendrite', c='red')
plt.ylabel('EPSP (mV)', fontsize = 12)
plt.legend()
      
### Soma and dendrite plot
plt.subplot(411)
plt.plot(mon[0].t[:] / ms, Vsoma_hack / mV, label='soma', c='black')
plt.plot(mon[0].t[:] / ms, mon[0].Vdend[:] / mV, label='dendrite', c='red')
plt.ylabel('EPSP (mV)', fontsize = 12)
plt.title('EPSP and EPSC with AMPA and NMDA: Poisson 45 neurons @ 10Hz', fontsize=16)
plt.legend()

# Currents Na and K plots
plt.subplot(413)
plt.plot(mon.t / ms, mon[0].INa[:] / pA, label='INa', c='blue')
plt.plot(mon.t / ms, mon[0].IK[:] / pA, label='IK', c='green')
plt.ylabel('EPSC (pA)', fontsize = 12)
plt.legend()

# NMDA and AMPA activity plots
plt.subplot(414)
plt.plot(mon.t / ms, mon[0].I_AMPA_ext[:] * fampa, label='AMPA', c='darkgoldenrod')
plt.plot(mon.t / ms, mon[0].I_NMDA_ext[:] * fnmda, label='NMDA', c='purple')
plt.xlabel('Time (ms)', fontsize = 12)
plt.ylabel('EPSC (pA)', fontsize = 12)
plt.legend()

plt.show()