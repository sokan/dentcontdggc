#!/usr/bin/env python3

###############################################################################
#                           AdaptiveIF model BRIAN2                           #
#                               active dendrite                               #
#                                    via                                      #
#                                custom events                                #
#                                with synapses                                #
#                        VALIDATION  (Negative Currents)                      #
###############################################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

# factor function used for normalization and min(=0), max(=1) during s_{AMPA,NMDA} graphs.
def factor(tau_rise,tau_decay):
    # Calculate the time @ peak
    tpeak = ((tau_decay*tau_rise) / (tau_decay-tau_rise)) * np.log(tau_decay/tau_rise)
    # Calculate the peak value - maximum value
    smax = ((tau_decay*tau_rise) / (tau_decay-tau_rise))*( np.exp(-(tpeak)/tau_decay) - np.exp(-(tpeak)/tau_rise) ) / ms
    
    return (1/smax)

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

start_scope()

## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -80 * mV
Csoma 	= 70.* pfarad
glsoma 	= (Csoma / (26.9 * ms)) / 2.3
Vreset  = -74. * mV
Vthres  = -56. * mV 

## dendrite
Eldend  = (Elsoma + 5 * mV)
gldend  = 2. * glsoma
Cdend   = 2. * Csoma

## Global
gc      = 6.8 * nS

## AdIF
alpha   = 1.35 * nS
tauw    = 45. * ms
beta    = .045 * nA 

## Na channels
tauNa   = 1. * ms

### AMPA (excitatory)
E_AMPA            = 0. * mV
g_AMPA_ext        = .3639 * nS 
tau_AMPA          = 2. * ms
tau_AMPA_rise     = .05 * ms
tau_AMPA_decay    = 1.5 * ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = .3746 * nS 
tau_NMDA_rise     = 2. * ms
tau_NMDA_decay    = 50. * ms 
Mg2               = .05
heta              = .2
gamma             = .062

## Model irrelevant
fampa  = factor(tau_AMPA_rise,tau_AMPA_decay)
fnmda  = factor(tau_NMDA_rise,tau_NMDA_decay)

## Validation/experiment parameters
tstart = 250 * ms
texprm = 500 * ms
tend   = 350 * ms
Iinj   = -.05 * nA 


###############################################################################
#                                M o d e l                                    #
###############################################################################

eqs_gc = '''
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma ) / Csoma : volt (unless refractory)    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + Iextdend - Isyn) / Cdend :volt
    dINa/dt = -INa / tauNa : amp
    dIK/dt = -IK / tauNa : amp
    
    Isyn = I_AMPA_ext + I_NMDA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext * fampa : amp
    ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + x_AMPA / ms : 1 # by using /ms we eradicate usage of alpha
    dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1
    
    I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + Mg2 / heta * exp(gamma * Vdend / mV)) * s_NMDA_ext * fnmda : amp
    ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + x_NMDA / ms: 1 
    dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 
    
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''

granule = NeuronGroup(1, model = eqs_gc, 
                      threshold = 'Vsoma > Vthres',
                      reset = 'Vsoma = Vreset; w += beta',
                      refractory= 2. * ms,
                      events = {
                          'dspike' : 'Vdend > -25 * mV and not allow_dspike',
                          'drop_dspike' : 'Vdend > -.09 * mV and not allow_dspike',
                          'force_true_dspike' : 'IK > -.009 * pA and not allow_dspike',
                      },
                      method = 'rk4'
                     )

# custom events conditions
granule.run_on_event('dspike', 'INa = 2500 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'IK = - 2500 * pA') #'; allow_dspike = True')
granule.run_on_event('force_true_dspike', 'allow_dspike = True')

###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################

# In this case we use PoissonInput for synaptic validation.

custom_rates = 0 * Hz
spike_inp = PoissonGroup(32, rates = 0 * Hz)

synAMPA = Synapses(spike_inp, 
                    granule,
                    on_pre = 'x_AMPA += 1',
                    delay = 3 * ms
                    )

synNMDA = Synapses(spike_inp, 
                   granule,
                   on_pre = 'x_NMDA += 1',
                   delay = 3 * ms
                   )

synAMPA.connect(p = 1.)
synNMDA.connect(p = 1.)

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

# recording variables, spikes and whatever needed.
spk = SpikeMonitor(granule)
mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'I_AMPA_ext', 'I_NMDA_ext', 's_AMPA_ext', 's_NMDA_ext', 'allow_dspike', "INa", "IK"), record=True)

spike_inp.rates = 0 * Hz
run(tstart)

spike_inp.rates = custom_rates
granule.Iextsoma = Iinj
granule.Iextdend = 0. * nA
run(texprm)

spike_inp.rates = 0 * Hz
granule.Iextsoma = 0. * nA
granule.Iextdend = 0. * nA
run(tend)

###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################

#print (mon[0].Vsoma)
Vsoma_vld = mon[0].Vsoma[:]

# Time data gathering needs to be a bit before the change of the curve.
tstartpoint = (250 * ms) / defaultclock.dt #in our experiment the injection starts at 300 ms.
Nstartpoint = int(tstartpoint) -1 # go 1 step BEFORE the start of injection. 
tendpoint = (750 * ms) / defaultclock.dt #in our experiment the injection stops at 1300 ms. Converted to time within the simulator.
Nendpoint = int(tendpoint) -1 # go 1 step BEFORE the stop of injection. 

## Tau membrane calculation ##
Vtau = abs(Vsoma_vld[Nendpoint:]) # Keep all the voltage needed for tau calculation.
Vnrm = (Vtau - min(Vtau)) / max(Vtau - min(Vtau)) # normalize to 1-0.
tau_m_debug = abs(Vnrm - 1/e) # used for debugging. 
tau_m = np.argmin(abs(Vnrm - 1/e)) * defaultclock.dt # 1/e = ~37%; argmin = index of minimum.
print ('Experimental tau membrane is : 26.9 ± 1.2 ms')
print ('Soma Membrane time constant in simulation is : ', tau_m)

# ## Rinput calculation ##
DV = Vsoma_vld[Nendpoint] / mV - Vsoma_vld[Nstartpoint] / mV
Rin = (DV * mV) / Iinj
print ('Experimental R input is : 228 ± 14.2 MΩ')
print('R input is :', Rin)

## Sag ratio calculation##
DV = Vsoma_vld[Nendpoint] / mV - Vsoma_vld[Nstartpoint] / mV
sag = DV / ( min(Vsoma_vld) / mV - Vsoma_vld[Nstartpoint] / mV )
print ('Experimental sag ratio is : 0.97 ± 0.01')
print('sag ratio is :', sag)

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

### Nicer somatic spikes
Vsoma_hack = mon[0].Vsoma[:]
for t in spk.t : 
      some_value = int(t / defaultclock.dt)
      Vsoma_hack[some_value] = 20 * mV

plt.figure(0)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='black')
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='red')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

plt.figure(1)
plt.tight_layout()
plt.plot(tau_m_debug, label='soma', c='black')
plt.hlines(1/e, 0, 7000, colors='green')
plt.plot(Vnrm, c='blue')
plt.xlabel('Index (Time * 10 ms)')
plt.ylabel('Normalized Membrane potential')
plt.legend()
plt.plot()

plt.show()