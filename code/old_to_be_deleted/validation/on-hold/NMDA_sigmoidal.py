#!/usr/bin/env python3

###############################################################################
#                           Plotting NMDA sigmoidal                           #
###############################################################################

import matplotlib.pylab as plt
import numpy as np

results = []
voltage = range(-90, 40)
Mg2     = .05 
heta    = .2 #5. #.2
gamma   = 0.062 #.04

#### NMDA sigmoidal ####
def nmda_sigmoid(v):
    return 1/(1 + Mg2 / heta * np.exp(-gamma * v))

for v in voltage:
    results.append(nmda_sigmoid(v))

plt.plot(voltage, results, label='sigmoidal')
plt.xlabel('Voltages (mV)')
plt.ylabel('Sigmoidal solutions according to Voltages')
plt.show()