# **"Dendritic contributions to place cell properties in the Dentate Gyrus Granule Cells"**

Here is the code that I've written for this project done at Poirazi lab, IMBB, FORTH, Heraklion, Crete, with postdoctoral reasearcher Spyros Chavlis as my supervisor.

Code has been run in a virtual environment via python3 -m venv.
You can replicate it by:
`pip install -r requirements.txt`

## Space reserved for further commens
