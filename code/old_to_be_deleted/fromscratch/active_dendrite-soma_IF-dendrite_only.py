#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

# Neuron parameters
## soma
Elsoma  = -87 * mV
glsoma  = 30. * nS 
Csoma   = 100. *pF 
Vreset  = -74. * mV
Vthres  = -56. * mV 

## dendrite
Eldend  = -82 * mV 
gldend  = 10. * nS
Cdend   = 250. * pF

gc      = 0 * nS

## AdIF
alpha   = 2. * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

## Na channels
tauNa   = 1. * ms

# Model
eqs = '''
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + Iext) / Cdend :volt
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w) / Csoma : volt
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    dINa/dt = -INa / tauNa : amp
    Iext : amp
    allow_dspike : boolean
    '''

neuron = NeuronGroup(1, model = eqs, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        events = {
                            'dspike' : 'Vdend > -25 * mV and allow_dspike',
                            'drop_dspike' : 'Vdend > 0 * mV and not allow_dspike'
                            },
                        method = 'rk4'
                        )

# custom events conditions
neuron.run_on_event('dspike', 'INa = 6550 * pA; allow_dspike = False')
neuron.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

# initializing all values.
neuron.Vsoma = Elsoma
neuron.Vdend = Eldend
neuron.allow_dspike = True

# recording variables, spikes and whatever needed.
mon = StateMonitor(neuron, ('Vsoma', 'Vdend', 'INa', 'allow_dspike'), record=True)
spk = SpikeMonitor(neuron)

# How the experiment is run.
run(10 * ms, report='text')
neuron.Iext = 3.65 * nA
run(5 * ms, report='text')
neuron.Iext = 0. * nA
run(50 * ms, report='text')
neuron.Iext = 1.65 * nA
run(5 * ms, report='text')

# plots that correspont to experiment results.

## Soma plot
#plt.subplot(31)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='blue')
#plt.xlabel('Time (ms)')
#plt.ylabel('Membrane potential (mV)')
#plt.legend()

## Dendrite plot
plt.subplot(312)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

## Na current plot
plt.subplot(311)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].INa / pA, label='INa', c='black')
plt.xlabel('Time (ms)')
plt.ylabel('VCurrent (pA)')

## boolean dspike plot
plt.subplot(313)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].allow_dspike, label='boolean dspike', c='orange')
plt.xlabel('Time (ms)')
plt.ylabel('True/False dspike')
plt.show()
