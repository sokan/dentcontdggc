#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

EPSP_measured = [0.5950,1.1784,1.7504,2.3113,2.8614,3.4008,3.9299,4.4489,4.9579,5.4574,5.9473,6.4281]
EPSP_expected = [0.5950, 0.5950 * 2, 0.5950 * 3, 0.5950 * 4, 0.5950 * 5, 0.5950 * 6, 0.5950 * 7, 0.5950 * 8, 0.5950 * 9, 0.5950 * 10, 0.5950 * 11, 0.5950 *  12]
synapse = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
x = np.linspace(0, 7, 50)
y1 = x
y2 = 1.44 * x

fig, ax1 = plt.subplots()

ax1.set_xlabel('Expected (mV)', fontsize = 12)
ax1.set_ylabel('Measured (mV)', fontsize = 12)

ax1.scatter(EPSP_measured, EPSP_expected, 
            label='somatic EPSP, active dendrites, AMPA and NMDA', c='blue'
            )

ax1.plot(x, y2,
         color = 'black',
         label = 'gain (y= 1.44 x)'
         )

ax1.plot(x, y1,
         color = 'gray',
         linewidth = 1.0,
         linestyle = '--',
         label = 'linear summation'
         )

ax1.set_xlim(xmin=0)
ax1.set_ylim(ymin=0)

plt.legend()

plt.show()