#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

EPSP_measuered = [0.59044098,1.16934572,1.73698171,2.29361042,2.83948674,3.37485915,3.89996992,4.41505524,4.92034536,5.41606478,5.90243235,6.37966144]
EPSP_expected = [0.59044098, 0.59044098 * 2, 0.59044098 * 3, 0.59044098 * 4, 0.59044098 * 5, 0.59044098 * 6, 0.59044098 * 7, 0.59044098 * 8, 0.59044098 * 9, 0.5904409 * 10, 0.59044098 * 11, 0.59044098 * 12]
synapse = [1,2,3,4,5,6,7,8,9,10,11,12]
x = np.linspace(0, 7, 50)
y1 = x
y2 = 1.44 * x

plt.plot()
plt.scatter(EPSP_measuered, EPSP_expected, 
            label='NMDA only, somatic EPSP, active dendrites', c='blue'
            )
plt.plot(x, y2,
         color = 'green',
         label = 'gain (y= 1.44 x)'
         )
plt.plot(x, y1,
         color = 'red',
         linewidth = 1.0,
         linestyle = '--',
         label = 'linear summation'
         )
plt.xlabel('Expected (mV)', fontsize = 12)
plt.ylabel('Measured (mV)', fontsize = 12)
plt.legend()

plt.show()
