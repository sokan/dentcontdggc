#!/usr/bin/env python3

###############################################################################
#                           AdaptiveIF model BRIAN2                           #
#                               active dendrite                               #
#                                    via                                      #
#                                custom events                                #
#                                with SYNAPSES                                #
#                        VALIDATION  (Positive Currents)                      #
###############################################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as np

# factor function used for normalization and min(=0), max(=1) during s_{AMPA,NMDA} graphs.
def factor(tau_rise,tau_decay):
    # Calculate the time @ peak
    tpeak = ((tau_decay*tau_rise) / (tau_decay-tau_rise)) * np.log(tau_decay/tau_rise)
    # Calculate the peak value - maximum value
    smax = ((tau_decay*tau_rise) / (tau_decay-tau_rise))*( np.exp(-(tpeak)/tau_decay) - np.exp(-(tpeak)/tau_rise) ) / ms
    
    return (1/smax)

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

start_scope()

## population
N_GC = 1 # GC neurons of DG

# ## soma
# Elsoma  = -87 * mV
# glsoma 	= 771.95 * psiemens #698. * psiemens
# Csoma 	= 22.907* pfarad #23. * pfarad 
# Vreset  = -74. * mV
# Vthres  = -56.81 * mV 
# print ('Soma Membrane time constant is : ', Csoma/glsoma)

# ## dendrite
# Eldend  = (Elsoma + 5 * mV) # -82 * mV 
# gldend  = 1.255 * glsoma #4 * glsoma # 610. * psiemens Default is: 1095.159 pS
# Cdend   = 1.31 * Csoma #2 * Csoma # 90. * pF Default is: 2737.89 pF
# print ('Dendrite membrane time constant is : ', Cdend/gldend)

# ## Global
# gc      = 2.3 * nS # 

# ## AdIF
# alpha   = 2. * nS # important to have the minor slow "lift-off of the curve"
# tauw    = 45. * ms
# beta    = .45 * nA # let's it reach threshold faster the higher it is.

## soma
Elsoma  = -87 * mV
Csoma 	= 125.* pfarad # Csoma 	= 67.* pfarad #23.907 * pfarad 
glsoma 	= (Csoma / (26.9 * ms)) / 2.5 # glsoma 	= 887. * psiemens #698.95 * psiemens (It needs to be ~30* > Csoma)
Vreset  = -74. * mV
Vthres  = -56.432 * mV 
print ('Soma Membrane time constant (Cm/gl) is : ', Csoma/glsoma)

## dendrite
Eldend  = (Elsoma + 5 * mV) # -82 * mV 
gldend  = 1.7 * glsoma #Decrease from 67.2 to 68.7 with 0.5 increase.
Cdend   = 1.7 * Csoma
print ('Dendrite membrane time (Cm/gl) constant is : ', Cdend/gldend)

## Global
gc      = 1.5 * nS # 4nS lower -> 4 mV higher. 0 leads to spike
# default -> ~67.2
# with alpha = 0 we have A LOT of spikes
# 5 ms tauw leads to 68.5 mV. 85 ms 65.8. Doesn't change much
# beta == 0 change practically. BUT when we DO have a spike it leads to anprecedented change in "getting back into steady state. VEEEERY long bellow.

## AdIF
alpha   = 1.35 * nS#1.8 * nS # sag ratio drops A LOT; Rinput drops A LOT when it goes higher.
tauw    = 45. * ms # lower -> higher sag. Rinp irrelevant
beta    = .045 * nA # doesn't change shit

## Na channels
tauNa   = 1. * ms

### AMPA (excitatory)
E_AMPA            = 0. * mV
g_AMPA_ext        = .3639 * nS 
tau_AMPA          = 2. * ms
tau_AMPA_rise     = .05 * ms
tau_AMPA_decay    = 1.5 * ms

### NMDA (excitatory)
E_NMDA            = 0. * mV
g_NMDA_ext        = .3746 * nS
tau_NMDA_rise     = 2. * ms
tau_NMDA_decay    = 50. * ms
Mg2               = .05
heta              = .2
gamma             = .062

## Model irrelevant
tstart = 600 * ms
tend   = 800 * ms
fampa  = factor(tau_AMPA_rise,tau_AMPA_decay)
fnmda  = factor(tau_NMDA_rise,tau_NMDA_decay)

Vpeak = []
f = []
Votagedend = []

## Current for validation experiments
currentIV = list(np.arange(-.05, .08, .01))
currentIf = list(np.arange(0.,40., 1.))
# currentIf = list(np.arange(0., .25, 0.1))
currentrheo = list(np.arange(-.05, 0.13, .01))

# Need to run different currents for curves creation.
for i in currentIf:
    
    start_scope()

###############################################################################
#                                M o d e l                                    #
###############################################################################
    
    eqs_gc = '''
        dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma ) / Csoma : volt (unless refractory)    
        dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
        
        dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + IK + Iextdend) / Cdend :volt
        dINa/dt = -INa / tauNa : amp
        dIK/dt = -IK / tauNa : amp
        
        Isyn = I_AMPA_ext + I_NMDA_ext : amp
        
        I_AMPA_ext = g_AMPA_ext * (Vdend - E_AMPA) * s_AMPA_ext * fampa : amp
        ds_AMPA_ext / dt = - s_AMPA_ext / tau_AMPA_decay + x_AMPA / ms : 1 # by using /ms we eradicate usage of alpha
        dx_AMPA / dt = - x_AMPA / tau_AMPA_rise : 1
        
        I_NMDA_ext = g_NMDA_ext * (Vdend - E_NMDA) / (1 + Mg2 / heta * exp(gamma * Vdend / mV)) * s_NMDA_ext * fnmda : amp
        ds_NMDA_ext / dt = - s_NMDA_ext / tau_NMDA_decay + x_NMDA / ms: 1 
        dx_NMDA / dt = - x_NMDA / tau_NMDA_rise : 1 
        
        Iextsoma : amp
        Iextdend : amp
        allow_dspike : boolean
        '''
    
    granule = NeuronGroup(1, model = eqs_gc, 
                         threshold = 'Vsoma > Vthres',
                         reset = 'Vsoma = Vreset; w += beta',
                         refractory= 2. * ms,
                         events = {
                             'dspike' : 'Vdend > -25 * mV and allow_dspike',
                             'drop_dspike' : 'Vdend > -.09 * mV and not allow_dspike',
                             'force_true_dspike' : 'IK > -.009 * pA and not allow_dspike',
                             },
                         method = 'rk4'
                         )
    
    # custom events conditions
    granule.run_on_event('dspike', 'INa = 2500 * pA; allow_dspike = False')
    granule.run_on_event('drop_dspike', 'IK = - 2500 * pA') #'; allow_dspike = True')
    granule.run_on_event('force_true_dspike', 'allow_dspike = True')
    
###############################################################################
#                             S Y N A P S E S                                 #
###############################################################################
    
    # In this case we use PoissonInput for synaptic validation.
    
    custom_rates = 0 * Hz
    spike_inp = PoissonGroup(32, rates = 0 * Hz)
    
    synAMPA = Synapses(spike_inp, 
                        granule,
                        on_pre = 'x_AMPA += 1',
                        delay = 3 * ms
                        )
    
    synNMDA = Synapses(spike_inp, 
                        granule,
                        on_pre = 'x_NMDA += 1',
                        delay = 3 * ms
                        )
    
    synAMPA.connect(p = 1.)
    synNMDA.connect(p = 1.)
    
    # initializing all values.
    granule.Vsoma = Elsoma
    granule.Vdend = Eldend
    granule.allow_dspike = True
    
###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################
    
    dt = defaultclock.dt / ms
    
    spk = SpikeMonitor(granule)
    
    # I want to record values AFTER 300 ms of simulation, because dendrite-soma 
    # membranes come to "equilibrium around then. It's easier to calculate min and max values
    # this way.
    mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'I_AMPA_ext', 'I_NMDA_ext', 's_AMPA_ext', 's_NMDA_ext', 'allow_dspike', "INa", "IK"), record=True)
    
    spike_inp.rates = 0 * Hz
    run(300 *ms)
    
    # recording variables, spikes and whatever needed.
    spike_inp.rates = custom_rates
    granule.Iextsoma = i * nA
    granule.Iextdend = 0. * nA
    run(1000 * ms)
    
    spike_inp.rates = 0 * Hz
    granule.Iextsoma = 0. * nA
    granule.Iextdend = 0. * nA
    run(700 * ms)
    
    # nstart      = int(tstart/defaultclock.dt)
    # nend        = int(tend/defaultclock.dt)
###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################
    tend = (1300 * ms) / defaultclock.dt
    Nend = int(tend - 1)
    tstart = (300 * ms) / defaultclock.dt
    Nstart = int(tstart - 1)
    
    # print (mon[0].Vsoma)
    # EPSP_amp = max(mon[0].Vsoma[nstart:nend]) - min(mon[0].Vsoma[nstart:nend])
    # print (max(mon[0].Vsoma), min(mon[0].Vsoma))
    # print ("AMPA and NMDA provokes a", EPSP_amp, "somatic EPSP.")
    # print ("ratio of I_NMDA/I_AMPA is: ", max(abs(mon[0].I_NMDA_ext)) / max(abs(mon[0].I_AMPA_ext)))
    # np.savetxt('synvalidationNMDA_dend.txt', mon[0].Vdend, header='data to use for doing syn validation (dendrite)')
    # np.savetxt('synvalidationNMDA_soma.txt', mon[0].Vsoma, header='data to use for doing syn validation (soma)')

## Vpeak array creation ##
    Vpeak.append(mon[0].Vsoma[Nend] / mV - mon[0].Vsoma[Nstart] / mV)

## Creating array f that holds all f of spikes ##
    f.append(spk.num_spikes)

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

#### Plots that correspond to experiment results. ####

### Nicer spikes 
    Vsoma_hack = mon[0].Vsoma[:]
    for t in spk.t:
        ii = int(t / defaultclock.dt)
        Vsoma_hack[ii] = 20*mV

# # Rheobase plot
#     plt.figure(1)
#     #plt.tight_layout()
#     plt.plot(mon.t / ms, Vsoma_hack / mV)
#     plt.xlabel('Time (ms)')
#     plt.ylabel('Membrane potential (mV)')
# # plt.plot([],[], linestyle = ' ', label = "Rheobase")  
# plt.plot([],[], linestyle = ' ', label = "Currents provided: -0.05 to 0.13 nA \nStepsize: 0.01 nA")
# plt.title('Rheobase')
# plt.legend(frameon = False)

# I-f plot
plt.figure(3)
plt.plot(currentIf, f, '*-')
plt.plot([],[], linestyle = ' ', label = "Currents provided: 0 to 90 nA \nStepsize: 1. nA")
plt.xlabel('Current injection (nA)')
plt.ylabel('Frequency (Hz)')
plt.title('f-I curve')
plt.legend()

# # I-V plot
# plt.figure(2)
# plt.plot(currentIV, Vpeak, 'o-', label="Currents provided: -0.05 to 0.08 nA\nStepsize: 0.01 nA")
# # plt.plot([],[], linestyle = ' ', label = "Currents provided: -0.05 to 0.08 nA\nStepsize: 0.01 nA")
# plt.xlabel('Current (nA)')
# plt.ylabel('Vpeak (mV)')
# plt.title('V-I curve')
# plt.legend()

# ### Nicer somatic spikes
# Vsoma_hack = mon[0].Vsoma[:]
# for t in spk.t : 
#       some_value = int(t / defaultclock.dt)
#       Vsoma_hack[some_value] = 20 * mV

# # plt.figure()
# # plt.plot()q
# plt.subplot(323)
# plt.tight_layout()
# plt.plot(mon[0].t[nstart:nend] / ms, Vsoma_hack[nstart:nend] / mV, label='soma', c='black')
# # plt.plot(mon[0].t[nstart:nend] / ms, mon[0].Vdend[nstart:nend] / mV, label='dendrite', c='red')
# # plt.vlines(453, -85.61975611, -86.21980416, colors='c',linestyles='dashed', label='amplitude')
# # plt.text(465, -86, '0.6 mV', fontsize = 10)
# # plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('EPSP (mV)', fontsize = 12)
# # plt.ylim([39.5, 41.5])
# # plt.text(400, 200, "Excited Soma at +40 mV membrane potential with one dendrite. Only NMDA active", fontsize = 20)
# # plt.ylim([-90, 125])
# plt.legend()

# plt.subplot(324)
# plt.tight_layout()
# plt.plot(mon[0].t[nstart:nend] / ms, mon[0].Vdend[nstart:nend] / mV, label='dendrite', c='red')
# # plt.vlines(453, -85.61975611, -86.21980416, colors='c',linestyles='dashed', label='amplitude')
# # plt.text(465, -86, '0.6 mV', fontsize = 10)
# # plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('EPSP (mV)', fontsize = 12)
# # plt.ylim([-41.5, -38])
# # plt.ylim([-90, 125])
# plt.legend()

      
# ### Soma and dendrite plot
# plt.subplot(311)
# plt.tight_layout()
# plt.plot(mon[0].t[:] / ms, Vsoma_hack / mV, label='soma', c='black')
# plt.plot(mon[0].t[:] / ms, mon[0].Vdend[:] / mV, label='dendrite', c='red')
# # plt.vlines(453, -85.61975611, -86.21980416, colors='c',linestyles='dashed', label='amplitude')
# # plt.text(465, -86, '0.6 mV', fontsize = 10)
# # plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('EPSP (mV)', fontsize = 12)
# plt.title('EPSP and EPSC with AMPA and NMDA: 1 neuron', fontsize=16)
# # plt.text(400, 200, "Excited Soma at +40 mV membrane potential with one dendrite. Only NMDA active", fontsize = 20)
# # plt.ylim([-90, 125])
# plt.legend()

# # plt.figure()
# # plt.plot()
# plt.subplot(313)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].INa[:] / pA, label='AMPA', c='blue')
# plt.plot(mon.t / ms, mon[0].IK[:] / pA, label='NMDA', c='green')
# plt.xlabel('Time (ms)', fontsize = 12)
# plt.ylabel('EPSC (pA)', fontsize = 12)
# # plt.ylim([-20, 0])
# plt.legend()

# # plt.figure()
# # plt.plot()
# # # plt.subplot(211)
# # # plt.tight_layout()
# # plt.plot(mon.t / ms, mon[0].s_AMPA_ext[:] * fampa, label='AMPA', c='blue')
# # plt.plot(mon.t / ms, mon[0].s_NMDA_ext[:] * fnmda, label='NMDA', c='red')
# # plt.xlabel('Time (ms)', fontsize = 12)
# # plt.ylabel('s variable', fontsize = 12)
# # plt.legend()

plt.show()