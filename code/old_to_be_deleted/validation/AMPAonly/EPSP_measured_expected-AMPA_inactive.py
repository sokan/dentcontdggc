#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

EPSP_measuered = [0.01830803,0.03660792,0.05489874,0.07318049,0.09145317,0.10971679,0.12797136,0.14621686,0.16445333,0.18268074,0.20089912,0.21910846]
EPSP_expected = [0.01830803, 0.01830803 * 2, 0.01830803 * 3, 0.01830803 * 4, 0.01830803 * 5, 0.01830803 * 6, 0.01830803 * 7, 0.01830803 * 8, 0.01830803 * 9, 0.01830803 * 10, 0.01830803 * 11, 0.01830803 * 12]
synapse = [1,2,3,4,5,6,7,8,9,10,11,12]
x = np.linspace(0, 0.25, 50)
y1 = x
y2 = 1.44 * x

plt.plot()
plt.scatter(EPSP_measuered, EPSP_expected, 
            label='AMPA only, somatic EPSP, with inactive dendrites', c='blue'
            )
plt.plot(x, y2,
         color = 'green',
         label = 'gain (y= 1.44 x)'
         )
plt.plot(x, y1,
         color = 'red',
         linewidth = 1.0,
         linestyle = '--',
         label = 'linear summation'
         )
plt.xlabel('Expected (mV)', fontsize = 12)
plt.ylabel('Measured (mV)', fontsize = 12)
plt.legend()

plt.show()
