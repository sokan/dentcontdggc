#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#             VALIDATION             #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

# Neuron parameters
## soma

### in case I want to add "geometry"
#diam_soma = 10. * um
#leng_soma = 10. * um
#area_soma = nm.pi * diam_soma * leng_soma
#glsoma    = 0.00003 * siemens / cm**2  * area_soma
#Csoma     = 1. * uF / cm**2 * area_soma 

### No geometry
Elsoma  = -87 * mV
glsoma 	= 698. * psiemens # 94. * psiemens vs 738.
Csoma 	= 23. * pfarad #3. * pfarad vs 24.
Vreset  = -74. * mV
Vthres  = -56.81 * mV 

tw = Csoma / glsoma

## dendrite

### in case I want to add "geometry"
#diam_dend = 1. * um
#leng_dend = 83. * um
#area_dend = nm.pi * diam_dend * leng_dend
#gldend    = 0.00001 * siemens / cm**2  * area_dend
#Cdend     = 2.5 * uF / cm**2 * area_dend

### No geometry ###
Eldend  = -82 * mV 
gldend  = 610. * psiemens # 26. * psiemens vs 1. * nS
Cdend   = 90. * pF # 7. * pF vs 150. * pF

gc      = 1.4 * nS

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

## Na channels
tauNa   = 1. * ms

# Global parameters
Iinj = -0.05 * nA 

# Model
eqs = '''
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + Iextdend) / Cdend :volt
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) + Iextsoma - w) / Csoma : volt (unless refractory)
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    dINa/dt = -INa / tauNa : amp
    Iextdend : amp
    Iextsoma : amp
    allow_dspike : boolean
    '''

neuron = NeuronGroup(1, model = eqs, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        refractory = 2 * ms,
                        events = {
                            'dspike' : 'Vdend > -25 * mV and allow_dspike',
                            'drop_dspike' : 'Vdend > 0 * mV and not allow_dspike'
                            },
                        method = 'rk4'
                        )

# custom events conditions
neuron.run_on_event('dspike', 'INa = 9600 * pA; allow_dspike = False')
neuron.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

# initializing all values.
neuron.Vsoma = Elsoma
neuron.Vdend = Eldend
neuron.allow_dspike = True

#pos_currents = list(np.arange(0, 2.5, 0.05))
#neg_currents = list(np.arange(-0.5, 0, 0.05)) 

#dt = defaultclock.dt / ms

# recording variables, spikes and whatever needed.
mon = StateMonitor(neuron, ('Vsoma', 'Vdend', 'INa'), record=True)
spk = SpikeMonitor(neuron)
   
#    print ("The external current is: ", round(i,2), "nA")
# How the experiment is run.Θα μπορούσα αλλά 
run(300 * ms)
neuron.Iextsoma = Iinj
neuron.Iextdend = 0. * nA
run(1000 * ms)
neuron.Iextsoma = 0. * nA
neuron.Iextdend = 0. * nA
run(100 * ms)
#neuron.Iextsoma = 1.65 * nA
#neuron.Iextdend = 0. * nA
#run(5 * ms, report='text')
#print ('current is', nm.round(Vpeak / mV, 2), 'nA')

####### Validation #######

Vsoma = mon[0].Vsoma[:]

print ('tw of soma is', tw)

#Vpeak = (Vsoma[12290] - min(Vsoma))
#print ('Vpeak is:', Vpeak)

### Positive current ###
#rheobase = 0.24 * nA # minimum current to produce one spike.
#V_I curve = from -400 * pA to rheobase
#f_I curve = I_s from 0 to 1 nA
# duration of current = 300 -> 1300

### Negative current ###
## Rinput ##
DV = Vsoma[12990] / mV - Vsoma[2990] / mV
Rin = (DV * mV) / Iinj

print('R input is:', Rin)

## Sag ratio ##
sag = DV / ( min(Vsoma) / mV - Vsoma[2990] / mV )

print('sag ratio is:', sag)

#### Plots that correspond to experiment results. ####
## Soma plot
#plt.subplot(411)
plt.figure()
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='blue')
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

## Dendrite plot
#plt.subplot(412)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
#plt.xlabel('Time (ms)')
#plt.ylabel('Membrane potential (mV)')
#plt.legend()

## Vpeak plot
#plt.subplot(413)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].Vpeak / mV, label='Vpeak', c='purple')
#plt.xlabel('Time (ms)')
#plt.ylabel('Vpeak (mV)')

## Na current plot
#plt.subplot(413)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].INa / pA, label='INa', c='black')
#plt.xlabel('Time (ms)')
#plt.ylabel('VCurrent (pA)')

## Dspike plot 
#plt.subplot(414)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].allow_dspike, label='boolean dspike', c='orange')
#plt.xlabel('Time (ms)')
#plt.ylabel('Vpeak')

plt.show()
