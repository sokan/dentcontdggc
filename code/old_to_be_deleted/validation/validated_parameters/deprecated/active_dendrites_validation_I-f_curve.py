#!/usr/bin/env python3

###############################################################################
#                           AdaptiveIF model BRIAN2                           #
#                               active dendrite                               #
#                                    via                                      #
#                                custom events                                #
#                                with SYNAPSES                                #
#                            VALIDATION (I-f curve)                           #
###############################################################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################
## soma

### in case I want to add "geometry"
#diam_soma = 10. * um
#leng_soma = 10. * um
#area_soma = nm.pi * diam_soma * leng_soma
#glsoma    = 0.00003 * siemens / cm**2  * area_soma
#Csoma     = 1. * uF / cm**2 * area_soma 

### No geometry
Elsoma  = -87 * mV
glsoma 	= 694. * psiemens # 94. * psiemens vs 738.
Csoma 	= 23. * pfarad #3. * pfarad vs 24.
Vreset  = -74. * mV
Vthres  = -56.81 * mV 

tw = Csoma / glsoma

## dendrite

### in case I want to add "geometry"
#diam_dend = 1. * um
#leng_dend = 83. * um
#area_dend = nm.pi * diam_dend * leng_dend
#gldend    = 0.00001 * siemens / cm**2  * area_dend
#Cdend     = 2.5 * uF / cm**2 * area_dend

### No geometry ###
Eldend  = -82 * mV 
gldend  = 610. * psiemens # 26. * psiemens vs 1. * nS
Cdend   = 90. * pF # 7. * pF vs 150. * pF

gc      = 1.4 * nS

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = .0045 * nA 

## Na channels
tauNa   = 1. * ms

# Global declarations
Vpeak = []
f = []
Voltagedend = []

###### Guidelines for validation of possitive currents ######
#rheobase = minimum current to produce one spike.
#V_I curve = from -400 * pA to rheobase
#f_I curve = I_s from 0 to 1 nA
# duration of current = 300 -> 1300

currentIf = list(np.arange(0., 2.251, 0.025))

###############################################################################
#                                M o d e l                                    #
###############################################################################
for i in currentIf:
    
    start_scope()        
# Model
    eqs = '''
        dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + Iextdend) / Cdend :volt
        dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) + Iextsoma - w) / Csoma : volt (unless refractory)
        dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
        dINa/dt = -INa / tauNa : amp
        Iextdend : amp
        Iextsoma : amp
        allow_dspike : boolean
        '''

    neuron = NeuronGroup(1, model = eqs, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        refractory = 4 * ms,
                        events = {
                            'dspike' : 'Vdend > -25 * mV and allow_dspike',
                            'drop_dspike' : 'Vdend > 0 * mV and not allow_dspike'
                            },
                        method = 'rk4'
                        )

# custom events conditions
    neuron.run_on_event('dspike', 'INa = 9600 * pA; allow_dspike = False')
    neuron.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

# initializing all values.
    neuron.Vsoma = Elsoma
    neuron.Vdend = Eldend
    neuron.allow_dspike = True

###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################

    dt = defaultclock.dt / ms
# recording variables, spikes and whatever needed.
    mon = StateMonitor(neuron, ('Vsoma', 'Vdend', 'INa', 'allow_dspike'), record=True)
    spk = SpikeMonitor(neuron)

    
# How the experiment is run.
    run(300 * ms)#, report='text')
    neuron.Iextsoma = i * nA
    neuron.Iextdend = 0. * nA
    run(1000 * ms)#, report='text')
    neuron.Iextsoma = 0. * nA
    neuron.Iextdend = 0. * nA
    run(100 * ms)#, report='text')
#    print ('External current is', round(i, 3), 'nA')

    # Nicer spikes 
    Vsoma = mon[0].Vsoma[:]
    for t in spk.t:
        ii = int(t / defaultclock.dt)
        Vsoma[ii] = 20*mV
        
    
###############################################################################
#                        D a t a     G a t h e r i n g                        #
###############################################################################
####### Validation #######
## Vpeak array creation ##
    Vpeak.append(mon[0].Vsoma[int(1299 / dt)] / mV - mon[0].Vsoma[int(299 / dt)] / mV)

## Creating array f that holds all f of spikes ##
    f.append(spk.num_spikes / 1)

###############################################################################
#                              P l o t t i n g                                #
###############################################################################

#### Plots that correspond to experiment results. ####
# Rheobase plot
    plt.figure(1)
    #plt.tight_layout()
    plt.plot(mon.t / ms, Vsoma / mV)
    plt.xlabel('Time (ms)')
    plt.ylabel('Membrane potential (mV)')
plt.plot([],[], linestyle = ' ', label = "Rheobase")  
plt.plot([],[], linestyle = ' ', label = "Currents provided: 0 to .08 nA \nStepsize: 0.01 nA")
plt.legend(frameon = False)

# I-f plot
plt.figure(3)
plt.plot(currentIf, f, '*-', label='I-f curve')
plt.plot([],[], linestyle = ' ', label = "Currents provided: 0 to 2.25 nA\nStepsize: 0.025 nA")
plt.xlabel('Current injection (nA)')
plt.ylabel('Frequency (Hz)')
plt.legend()

# I-V plot
plt.figure(2)
plt.plot(currentIf, Vpeak, 'o-', label='I-V curve')
plt.plot([],[], linestyle = ' ', label = "Currents provided: -0.05 to 0.08 nA\nStepsize: 0.01 nA")
plt.xlabel('Current (nA)')
plt.ylabel('Vpeak (mV)')
plt.legend()

## Dendrite plot
#plt.subplot(412)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
#plt.xlabel('Time (ms)')
#plt.ylabel('Membrane potential (mV)')
#plt.legend()

## Vpeak plot
#plt.subplot(413)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].Vpeak / mV, label='Vpeak', c='purple')
#plt.xlabel('Time (ms)')
#plt.ylabel('Vpeak (mV)')

## Na current plot
#plt.subplot(413)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].INa / pA, label='INa', c='black')
#plt.xlabel('Time (ms)')
#plt.ylabel('VCurrent (pA)')

## Dspike plot 
#plt.subplot(414)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].allow_dspike, label='boolean dspike', c='orange')
#plt.xlabel('Time (ms)')
#plt.ylabel('Vpeak')

plt.show()
