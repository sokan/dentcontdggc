#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#           with SYNAPSES            #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

# Neuron parameters

## population
N = 1000
N_GC = int(N * 0.8) # GC neurons of DG

## soma
Elsoma  = -87 * mV
glsoma 	= 698. * psiemens
Csoma 	= 23. * pfarad
Vreset  = -74. * mV
Vthres  = -56.81 * mV 

## dendrite
Eldend  = -82 * mV 
gldend  = 610. * psiemens
Cdend   = 90. * pF

## Global
gc      = 1.4 * nS

## Synapse_Global
Esyn = 0. * mV

### AMPA
g_AMPA_ext = 2.08 * nS
#g_AMPA_rec = 0.104 * nS * 800.
tau_AMPA = 2. * ms

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

## Na channels
tauNa   = 1. * ms

# AMPA (excitation)


# Model
eqs_gc = '''
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + Iextdend - Isyn) / Cdend :volt
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma) / Csoma : volt (unless refractory)
    
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    
    dINa/dt = -INa / tauNa : amp
    
    Isyn = I_AMPA_ext : amp
    
    I_AMPA_ext = g_AMPA_ext * (Vdend - Esyn) * s_AMPA_ext : amp
    ds_AMPA_ext/ dt = - s_AMPA_ext / tau_AMPA : 1
    
    Iextsoma : amp
    Iextdend : amp
    allow_dspike : boolean
    '''

granule = NeuronGroup(N_GC, model = eqs_gc, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        refractory= 2. * ms,
                        events = {
                            'dspike' : 'Vdend > -25 * mV and allow_dspike',
                            'drop_dspike' : 'Vdend > 0 * mV and not allow_dspike'
                            },
                        method = 'rk4'
                        )

eqs_pre_ext = '''
    s_AMPA_ext += 1
    '''

indices = array([0, 1, 2, 3, 4, 5, 6, 7, 8])
time = array([15, 100, 110, 112, 114, 200, 202, 204, 206]) * ms
inp = SpikeGeneratorGroup(N_GC, indices, time)

# Synapses
syns = Synapses(inp, granule,
                on_pre = eqs_pre_ext,
                method = 'rk4'
                )

syns.connect('i != j')

# custom events conditions
granule.run_on_event('dspike', 'INa = 1600 * pA; allow_dspike = False')
granule.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

# initializing all values.
granule.Vsoma = Elsoma
granule.Vdend = Eldend
granule.allow_dspike = True

# recording variables, spikes and whatever needed.
mon = StateMonitor(granule, ('Vsoma', 'Vdend'), record=True)
spk = SpikeMonitor(granule)

# How the experiment is run.
run(10 * ms, report='text')
#granule.Iextsoma = 0. * nA
#granule.Iextdend = 1.1 * nA
run(500 * ms, report='text')
#granule.Iextsoma = 0. * nA
#granule.Iextdend = 0. * nA
run(50 * ms, report='text')
#granule.Iextsoma = 0. * nA
#granule.Iextdend = 0. * nA
#run(5 * ms, report='text')

# Plotting

## Soma plot
plt.plot(1)
#plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vsoma / mV, label='soma', c='blue')
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='green')
plt.xlabel('Time (ms)')
plt.ylabel('Membrane potential (mV)')
plt.legend()

### Na current plot
#plt.subplot(312)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].INa / pA, label='INa', c='black')
#plt.xlabel('Time (ms)')
#plt.ylabel('Na Current (pA)')
#
### boolean dspike plot
#plt.subplot(313)
#plt.tight_layout()
#plt.plot(mon.t / ms, mon[0].allow_dspike, label='boolean dspike', c='orange')
#plt.xlabel('Time (ms)')
#plt.ylabel('True/False dspike')


plt.show()