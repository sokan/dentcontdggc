#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

# Neuron parameters
## soma
Elsoma  = -87 * mV
glsoma 	= 698. * psiemens
Csoma 	= 23. * pfarad
Vreset  = -74. * mV
Vthres  = -56.81 * mV 

## dendrite
Eldend  = -82 * mV 
gldend  = 610. * psiemens
Cdend   = 90. * pF

gc      = 1.4 * nS

## AdIF
alpha   = 1.6 * nS
tauw    = 45. * ms
beta    = 0.045 * nA 

## Na channels
tauNa   = 1. * ms

# Model
eqs = '''
    dVdend/dt = (-gldend * (Vdend - Eldend) -gc * (Vdend - Vsoma) + INa + Iext) / Cdend :volt
    dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w ) / Csoma : volt
    dw/dt = (alpha * (Vsoma -Elsoma) -w ) / tauw : amp
    dINa/dt = -INa / tauNa : amp
    Iext : amp
    allow_dspike : boolean
    '''

neuron = NeuronGroup(1, model = eqs, 
                        threshold = 'Vsoma > Vthres',
                        reset = 'Vsoma = Vreset; w += beta',
                        events = {
                            'dspike' : 'Vdend > -25 * mV and allow_dspike',
                            'drop_dspike' : 'Vdend > 0 * mV and not allow_dspike'
                            },
                        method = 'rk4'
                        )

# custom events conditions
neuron.run_on_event('dspike', 'INa = 2800 * pA; allow_dspike = False')
neuron.run_on_event('drop_dspike', 'Vdend = -35 * mV; allow_dspike = True')

# initializing all values.
neuron.Vsoma = Elsoma
neuron.Vdend = Eldend
neuron.allow_dspike = True
neuron.Iext = 0 * nA

# recording variables, spikes and whatever needed.
mon = StateMonitor(neuron, ('Vsoma', 'Vdend', 'INa', 'allow_dspike', 'Iext'), record=True)
spk = SpikeMonitor(neuron)

# How the experiment is run.
run(1 * ms, report='text')
neuron.Iext = 1.1 * nA
run(5 * ms, report='text')
neuron.Iext = 0. * nA
run(13 * ms, report='text')
# neuron.Iext = 1.65 * nA
# run(5 * ms, report='text')

    
# Nicer spikes 
Vsoma = mon[0].Vsoma[:]
for t in spk.t:
    i = int(t / defaultclock.dt)
    Vsoma[i] = 20*mV

# plots that correspont to experiment results.

## Soma plot
# plt.subplot(211)
# plt.tight_layout()
# plt.plot(mon.t / ms, Vsoma / mV, label='soma', c='blue')
# plt.xlabel('Time (ms)', fontsize = 10)
# plt.ylabel('Membrane potential (mV)', fontsize = 10)
# plt.legend()

## Dendrite plot
plt.subplot(211)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Vdend / mV, label='dendrite', c='red')
plt.xlabel('Time (ms)', fontsize = 10)
plt.ylabel('Membrane potential (mV)', fontsize = 10)
plt.legend()

## Na and external current plot
# plt.subplot(313)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].INa / pA, label='INa', c='blue')
# plt.plot(mon.t / ms, mon[0].Iext / nA, label='Iext', c='red')
# plt.xlabel('Time (ms)', fontsize = 10)
# plt.ylabel('VCurrent (pA)', fontsize = 10)
# plt.legend()

# ## Iinput current plot
plt.subplot(212)
plt.tight_layout()
plt.plot(mon.t / ms, mon[0].Iext / nA, label='Iext', c='red')
plt.xlabel('Time (ms)', fontsize = 10)
plt.ylabel('VCurrent (pA)', fontsize = 10)
plt.legend()

# ## boolean dspike plot
# plt.subplot(414)
# plt.tight_layout()
# plt.plot(mon.t / ms, mon[0].allow_dspike, label='boolean dspike', c='orange')
# plt.xlabel('Time (ms)', fontsize = 10)
# plt.ylabel('True/False dspike', fontsize = 10)
# plt.legend()

plt.show()