#!/usr/bin/env python3

######################################
#       AdaptiveIF model BRIAN2      #
#           active dendrite          #
#                via                 #
#           custom events            #
#             FIG6C_Kim              #
######################################

from brian2 import *
import matplotlib.pyplot as plt
import numpy as nm

###############################################################################
#                 N e u r o n             p a r a m e t e r s                 #
###############################################################################

## population
N_GC = 1 # GC neurons of DG

## soma
Elsoma  = -80 * mV
Csoma 	= 70.* pfarad
glsoma 	= (Csoma / (26.9 * ms)) / 2.3
Vreset  = -74. * mV
Vthres  = 56.432 * mV 
print ('Soma Membrane time constant (Cm/gl) is : ', Csoma/glsoma)

## dendrite
Eldend  = (Elsoma + 5 * mV) 
gldend  = 2. * glsoma 
Cdend   = 2. * Csoma
print ('Dendrite membrane time (Cm/gl) constant is : ', Cdend/gldend)

## Global
gc      = 6.8 * nS  

## AdIF
alpha   = 1.35 * nS 
tauw    = 45. * ms  
beta    = .045 * nA 

## Na channels
tauNa   = 3. * ms
tauK = 6. * ms

dt = defaultclock.dt


# Global declarations
Vpeak_soma = []
Vpeak_dend = []
f = []
Iinj_tot=[]
vmdendall = {} # dictionary that will keep all currents and all voltages produced by each

eqs = '''
      dVsoma/dt = (-glsoma * (Vsoma - Elsoma) - gc * (Vsoma - Vdend) - w + Iextsoma) / Csoma : volt (unless refractory)
      dw/dt = (alpha * (Vsoma -Elsoma) - w ) / tauw : amp
    
      dVdend/dt = (-gldend * (Vdend - Eldend) - gc * (Vdend - Vsoma) + INa + IK + Iextdend) / Cdend : volt 
      dINa/dt = -INa / tauNa : amp
      dIK/dt = -IK / tauK : amp
       
      Iextdend : amp
      Iextsoma : amp
      allow_dspike : boolean
      '''

###############################################################################
#                                M o d e l                                    #
###############################################################################
for Iinj in range (0, 1200, 100):  
    
    granule = NeuronGroup(1, model = eqs,
                     threshold = 'Vsoma > Vthres',
                     reset = 'Vsoma = Vreset; w += beta',
                     refractory = 2 * ms,
                     events = {
                           'dspike' : 'Vdend > -55 * mV and allow_dspike',
                           'drop_dspike' : 'Vdend > -37 * mV and not allow_dspike',
                           'force_true_dspike' : 'IK > -.009 * pA and not allow_dspike'
                           },
                     method = 'rk4'
                     )
    
    # custom events conditions
    granule.run_on_event('dspike', 'INa = 1000 * pA; allow_dspike = False')
    granule.run_on_event('drop_dspike', 'IK = - 1600 * pA') #'; allow_dspike = True')
    granule.run_on_event('force_true_dspike', 'allow_dspike = True')
    
    # initializing all values.
    granule.Vsoma = Elsoma
    granule.Vdend = Eldend
    granule.allow_dspike = True
    
###############################################################################
#                             M O N I T O R I N G                             #
###############################################################################
    
     
    # recording variables, spikes and whatever needed.
    spk = SpikeMonitor(granule)
          
    # How the experiment is run.
    t1=200*ms # time needed for equilibrium between dendrite and soma.
    tstim = 5 * ms
    t2 = 95 * ms
    
    
    mon = StateMonitor(granule, ('Vsoma', 'Vdend', 'INa', 'IK', 'allow_dspike'), record=True)
    
    run(t1)
    
    granule.Iextsoma = 0. * pA
    granule.Iextdend = Iinj * pA
    run(tstim)
    
    granule.Iextsoma = 0. * pA
    granule.Iextdend = 0. * pA
    run(t2)

    Vdendmon = mon[0].Vdend[:]
          
    ## Vpeak array creation ##
    Vpeak_soma.append(max(mon[0].Vsoma[:]) / mV - min(mon[0].Vsoma[200:]) / mV)
    Vpeak_dend.append(max(Vdendmon) / mV + 55) # 55 is dendrite threshold.
    
    for value in range(len(Vpeak_dend)):
        if Vpeak_dend[value] < 0 :
            Vpeak_dend[value] = 0 * mV
    Iinj_tot.append(Iinj)

###############################################################################
#                              P l o t t i n g                                #
###############################################################################
      
fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('Current intensity (pA)', fontsize = 12)
ax1.set_ylabel('Peak amp. (mV)', fontsize = 12, color=color)
ax1.set_ylim(-1.5, 25)
ax1.plot(Iinj_tot, Vpeak_dend,
         'o-',
         color = color,
         label='Dendrite',
         )

ax2 = ax1.twinx() # instantiate a second axes that shares the same x-axis

color = 'tab:black'
ax2.set_ylabel('Peak amp. (mV)', fontsize = 12) # we already handled the x-label with ax1
ax2.set_ylim(-1.5, 50)
ax2.plot(Iinj_tot, Vpeak_soma,
         '^-',
         color = 'black',
         label = 'soma',
         )

fig.legend(ncol=2, loc='upper right', bbox_to_anchor=(.53, .95, ))

fig.tight_layout() 
plt.show()